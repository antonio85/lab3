

import java.util.Scanner;

public class lab3 {
    public static void main(String[] args) {
    
    	// Loop over the Scanner
        // Split each line into the task and the corresponding number (if one exists)
        // Depending on what the input task was, preform the corresponding function
        //      (ie) insert, maximum, extractMax, isEmpty, or print
        // Hint: Use a switch-case statement

        // Don't forget to close your Scanner!
        
        
        Scanner scan= new Scanner(System.in);
        int times = scan.nextInt();
        pQueue heap = new pQueue(times);
        String input;
        String task;

        
        
        for(int i = 0; i<=times; i++){
            
            input = scan.nextLine();
            String[] phrases = input.split(" ");
            task = phrases[0];

            

            switch(task) {
            	case "insert":
            		heap.insert(Integer.parseInt(phrases[1]));
            		break;
                case "print":
                        heap.print();
                        break;
                case "isEmpty":
                        if(heap.isEmpty())
                            System.out.println("Empty");
                        else
                            System.out.println("Not Empty");
                            
                        break;
                case "maximum":
                        heap.maximum();
                        break;
                case "extractMax":
                        heap.extractMax();
                        break;
                case "build":
                        
//                        String linea = phrase[1]
//                        String build = phrases[1].substring(1, phrases[1].length()-1);
//                        String cut = build.split(",");
//                        System.out.println(build);
                        String temp = phrases[1].substring(1, phrases[1].length()-1);
  
                        String [] build = temp.split(",");
                        Integer [] array = new Integer[build.length];
                        
                        for(int j = 0;j<array.length;j++){
                            array[j]=Integer.parseInt(build[j]);
                        }
//                        
//                        
//                        for(int j = 0;j<array.length;j++){
//                            System.out.print(array[j]);
//                        }
//                        
                        heap.build(array);

                        break;

            }
        }
        scan.close();
   

//        MaxHeap temp = new MaxHeap(100);
//        Integer [] arr = {1,2,3,4,5,6,7,8,9};
//        
//        temp.buildHeap(arr);
        
        
    }

}

