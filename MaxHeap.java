

import java.util.Comparator;
import java.lang.Math;

public class MaxHeap<E extends Comparable> {
    private E[] myArray;
    private int maxSize;
    private int length;

    public MaxHeap(int s){
    	// Build the constructor
        length = 0;
        maxSize = s;
        this.myArray = (E[])(new Comparable[s]);
    }

	// helper functions
    public E[] getArray(){
        return myArray;
    }

    public void setArray(E[] newArray){
    	if (newArray.length > maxSize){
    		return;
    	}
        myArray = newArray;
        length = newArray.length-1;
    }

    public int getMaxSize(){
        return maxSize;
    }

    public void setMaxSize(int ms){
        maxSize = ms;
    }

    public int getLength(){
        return length;
    }

    public void setLength(int l){
        length = l;
    }

    // Other Methods
    public void insert(E data){
    
    	// Insert an element into your heap.
    	
    	// When adding a node to your heap, remember that for every node n, 
    	// the value in n is less than or equal to the values of its children, 
    	// but your heap must also maintain the correct shape.
		// (ie there is at most one node with one child, and that child is the left child.)
		// (Additionally, that child is farthest left possible.)
        if(length>maxSize){// maxsize overpass
            return;
        }        
        
        length++;
        myArray[length-1]= data;
        
        heapify(1);
        //print();
        //System.out.println();
        
    }

    public Comparable<E> maximum(){
        // return the maxi value in the heap
        System.out.println(myArray[0]);
        return myArray[0];
    }

    public Comparable<E> extractMax(){
        // remove and return the mai value in the heap
        E temp = myArray[0];
        System.out.println(temp);
        heapify(2);
        return temp;
        
    }
    
    public void heapify(int i){
    	// helper function for reshaping the array
        if(i==1){
            
        int lastAdded = length-1;
            
            
            while(myArray[lastAdded].compareTo(myArray[((lastAdded-1)/2)])==1){
                if(myArray[lastAdded].compareTo(myArray[((lastAdded-1)/2)])==1)
                {
                    swap(lastAdded, (lastAdded-1)/2);
                    lastAdded = (lastAdded-1)/2;
                }
                else
                {
                    return;
                }
            }
        }
        else if(i==2)
        {
            if(length==0){
                return;
            }
            int lastAdded = length-1;
            myArray[0] = myArray[lastAdded];
            int top = 0;
            int higher;
            
            length--;
            
            while(true)
            {
   
                int leftChild = 2*top+1;
                int rightChild = 2*top+2;
                
                if(leftChild>length-1 && rightChild>length-1)
                {
                    return;
                }
                
                if(myArray[leftChild].compareTo(myArray[rightChild])==1){
                   higher = leftChild;
                }
                else
                {
                   higher = rightChild;
                }
                
                
                
                
                
                if(top>-1 && myArray[higher].compareTo(myArray[top])==1)
                {
                    swap(higher, top);
                    top = higher;
                }
                else
                {
                    return;
                }
            }

        }
    }
    
    public void swap(int index1,int index2 ){
        E temp = myArray[index1];
        
        myArray[index1]=myArray[index2];
        myArray[index2]=temp;
        
    }    
    
    public void swap2(E []arr, int index1,int index2 ){
        E temp = arr[index1];
        
        arr[index1]=arr[index2];
        arr[index2]=temp;
        
    }
    
    public void print(E[] newArr){

                        
                    System.out.print("Current Queue: ");
                     for(int i = 0; i<newArr.length; i++){
            
                             System.out.print(newArr[i]);

                            if(i<newArr.length-1){    
                                System.out.print(",");
                            }
                        }
                    System.out.println();
    }
    
    public void buildHeap(E[] newArr){
		// used for the extra credit    
                int longitud = (newArr.length) ;//index of the last added
//                System.out.println("longitud "+longitud);
                int lastAdded = longitud-1;
                int parentLastAdded = (lastAdded-1)/2;
//                System.out.println("parent of the last " +parentLastAdded);
                int higher;
                int high;
                
                
                //print(newArr);
                while(parentLastAdded!=-1){
                    
                    int leftChild = (parentLastAdded*2)+1;
                    int rightChild = (parentLastAdded*2)+2;
//                    System.out.println("left " +leftChild);
//                    System.out.println("right " + rightChild);
                    
//                    if(newArr[leftChild].compareTo(newArr[rightChild])==1){
//                         higher = leftChild;
//                    }
//                    else
//                    {
//                       higher = rightChild;
//                    }
                    
                    higher = (newArr[leftChild].compareTo(newArr[rightChild])==1)?leftChild:rightChild;
                            
                    
//                    System.out.println("higher " + higher);
                    
                    
                    if(newArr[higher].compareTo(newArr[parentLastAdded])==1){
                        swap2(newArr, higher, parentLastAdded);
                        
//                        System.out.print("Swap ");
                        //print(newArr);
                        int parent = higher;
//                        System.out.println("parent" +parent);
                       
                        if(parent*2+2<newArr.length){
//                            System.out.println("DOBLE ");
                            
                            while(parent*2+1<newArr.length){
                                int left = (parent*2)+1;
                                int right= (parent*2)+2;
//                                System.out.println("Triple "+ parent);
//                               if(newArr[leftChild].compareTo(newArr[rightChild])==1){
//                                    high = leftChild;
//                               }
//                               else
//                               {
//                                  high = rightChild;
//                               }
                                high = (newArr[left].compareTo(newArr[right])==1)?left:right;
                                
                                
//                                System.out.println("doble swap left " +left + ", right " + right + ", high " + high);
                                
                                if(newArr[high].compareTo(newArr[parent])==1){
                                    swap2(newArr,high, parent);
                                }
                                parent = high;    
                                }
                                
                            }
                    }
                    
                     parentLastAdded--;
//                     System.out.println("parentlast" + parentLastAdded);
                    
//                    System.out.print("Final :");
                    print(newArr);
                }
                    
                
              
                
                
                
                
                
	}
    

}
